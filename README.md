# Demo-Perl-Mojo

A demonstration Perl Mojolicious application.


## Running the app

```sh
carton exec -- script/demo_pet_store daemon
```
